# print([i for i in range(1,132)]) 


'''
n = int(input())
print([ i for i in range(-1,2,2)] * n) 
'''

'''
from random import randint
print([randint(-100, 100) for i in range(10)])  
'''

'''
print([i for i in range(-15,16) if i % 5 == 0])
'''

'''
print([i for i in range(2,51) if i % 2 == 0])
print([i for i in range(2,51) if i % 2 != 0])
'''

from random import randint
print([randint(-10, 40) for i in range(5)])

from random import randint
print([randint(-20, 30) for i in range(7)])
