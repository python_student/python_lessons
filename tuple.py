"""
numbers = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
print(sum(numbers))
"""

"""
long_word = (
    'т','т','а', 'и','и','а','и',
    'и', 'и','т','т','а','и','и',
    'и','и','и','т','и'
    )

print("Количество 'т':", long_word.count('т'))
print("Количество 'а':", long_word.count('а'))
print("Количество 'и':", long_word.count('и'))
"""

'''
week_temp = (26, 29, 34, 32, 28, 26, 23)
sum_temp = sum(week_temp)
days = len(week_temp)
mean_temp = sum_temp / days
print(int(mean_temp))
'''

'''
p =('+792345678', '+792345478', '+792355678', '+592345678', '+392345678', '+7923456558')
for i in p:
    if i[:2] =="+7":
        print(i)
'''
a = "Оценки: 5, 4, 3, 2, 4, 5, 4"
b = a.translate({ord(i): None for i in '" ",:йЙцЦуУкКеЕнНггШшщЩзЗхХъЪфФыывваапПрРоОлЛдДжЖэЭяЯчЧсСмМиИтТьЬбБюЮ'})
print(tuple(b))